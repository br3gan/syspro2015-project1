
#ifndef _LIST_H_
#define _LIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct List{
     
     char* word;
     struct List* next;
     
}List;

//creates a knob with a curtain word and adds it at the end of the List
//returns 0 at success
//returns -1 at failure
int add_tail( List* last , char* word );

//delete the first knob, sets nextone as new head
List* remove_head( List* head );

//prints List - not used in this program
void print_List( List* head );

//frees all the memory allocated to store the List
void free_List( List* head );

//takes a sequence of words and set each one as index of new knobs at the end of the List
List* add_new_word_list_L( List* last , char** words_array );

//initializes the List with a given word
List* initialize_List( List* head , char* word );


#endif