//This file is in sync 

#include "header.h"

//called when oracle returns NULL pointer
//prints the structure where the word was saved in (tree/list)
//used desides if the tree will be printed
void word_found( char* word , char* where , int seed , Tree* root){
     
     printf( "Word (*%s*) was found in %s (%d) \n" , word , where , seed );

     if( deside( "Wonna print the Tree?" ) )
          print_Tree( root );
     
     return;
}

//asks user a question. Returns 1 or 0,
//if answer is yes or no respectively
int deside( char* message ){
     
     char deside;
     
     printf( "%s (Y/N)\n" , message );
     
     deside = getchar();
     
     //take valid answer
     while( deside != 'Y' && deside != 'y' && deside != 'N' && deside != 'n' ){
     
          printf("\tdidnt catch that\n answer: (Y/N)\n");
          deside = getchar();
     }
     
     if( deside == 'N' || deside == 'n' ){
          printf("see ya then\n");
          return 0;
     }
     
     return 1;
}

//allocates memory for the array returned from oracle.
//new array contains not duplicates of a word, nor a word
//which hashes already exist in the filter 
char** filter_words_array( char** words_array , char* filter , int num_of_hashes , unsigned int SIZE ){
     
     //means the search is over
     if( words_array == NULL ){
          return NULL;
     }
     
     //if not, create an array
     char** filtered_words_array = NULL;
     
     int i = 0; int j = 0;
     
     //fill it with words from oracle's output
     while( words_array[i] != NULL ){
          
          //only if their hashes are not already in the filter.
          if( !contain_in_filter( filter , num_of_hashes , SIZE , words_array[i] ) ){
               
               //(first call :)allocate memory for an array of pointers
               //(next calls :)reallocate the memory to fit one more pointer
	       	
		/////////////////////////////////////
		//WARNING: realloc is not a good idea
		/////////////////////////////////////

               filtered_words_array = (char**)realloc( filtered_words_array , ( j + 1 ) * sizeof(char*) );
     
               //allocate memory for each new array
               filtered_words_array[j] = (char*)malloc( strlen(words_array[i]) + 1 );
          
               //copy word to own memory
               strcpy( filtered_words_array[j] , words_array[i] );
               
               //num of unique words 
               j++;
          }
          
          //num of woeds returned by oracle
          i++;
     
     }
     
     //add a NULL pointer at the end of array of pointers
     filtered_words_array = (char**)realloc( filtered_words_array , ( j + 1 ) * sizeof(char*) );
     
     filtered_words_array[j] = NULL;
     
     //at this point, if all words were already in the tree/list
     //function returns an array of pointers with a single poiter, which is NULL.
     //this means that the knob whose word trigered this situation has to be a leaf.
     return filtered_words_array;
}

//prints all the arrays in filtered_words_array
//not used in program
void print_filtered_words( char** filtered_words_array ){
     
     int i = 0;
     
     if( filtered_words_array == NULL )
          return;
     
     while( filtered_words_array[i] != NULL ){
          
          printf("*%s*\n", filtered_words_array[i] );
          i++;          
     }
     
     printf("NULL\n");
}

//frees the memory allocated by oracle's copied output
void free_filtered_words( char** filtered_words_array ){
     
     int i = 0;
     
     if( filtered_words_array == NULL )
          return;
     
     while( filtered_words_array[i] != NULL ){
          
          free( filtered_words_array[i] );
          i++;
          
     }
     
     free(filtered_words_array);
}

//trigures all the free functions - frees all the allocated momory
void free_all( Tree* root , List* head , char* filter , char** words_array ){

     free_filtered_words( words_array );
     free_List( head );
     free_Tree( root );
     free(filter);
}

//if exit_code is negative value, frees all the memory and exits the program
void error_check( int exit_code , Tree* root , List* head , char* filter , char** words_array ){
     
     if ( exit_code < 0 ){
          printf(" will now exit \n");
          free_all( root , head , filter , words_array );
          exit(-1);
     }    
}

int main( int argc , char** argv ){
     
/////////////////////////////////////////////////////////INITIALIZATIONS//////////////////////////////////////////////////  
     
     //terminal inputs
     int num_of_hashes = 3;     //expects original number - nonzero [1-n]. Is 3 by default.
     unsigned int SIZE = 0;     //expects number of bytes
     int DEPTH = 0;             //expects possitive number
     
     int seed = 43;              //random seed              
     int pos = 1;               //number of argument
     
     //if there are 5 arguments, it means optional -k is given.
     //Do the error checks incase no flag was found.
     //if there are 3 arguments, it means optional -k is not given
     //At wrong input  print the correct form of input and exit program.
     if( !( ( argc == 5 && strcmp( argv[1] , "-k" ) == 0 ) || ( argc == 3 ) ) ){
          printf("Incorrect data. Expect arguments in form:\n 1: -k [number_of_hash_functions] (optional)\n 2: [size_of_filter]\n 3: [tree_depth]\n");
          return -1;
     }
     
     //case of optional '-k'
     //assign number of hash functions given by user
     if( pos < argc )
          if( strcmp(argv[pos] , "-k") == 0 ){
               pos++;
               num_of_hashes = atoi( argv[pos++] );
          }
     
     //first forced argument - Size of Filter
     if( pos < argc )
          SIZE = (unsigned int)atoi( argv[pos++] );
     
     //second forced argument - Depth of Tree
     if( pos < argc )
          DEPTH = atoi( argv[pos++] );
     
     //etc
     char** words_array = NULL;                 //store words which were not contained in the filter
     char** raw_words_array = NULL;             //store raw output form oracle
     
     int exit_code = 0;                         //initialize error code as 0 (no error)
     
     //oracle
     initSeed(seed);                            //initialize oracle
     
     //tree   
     Tree* root = NULL;                         //create root of the Tree
     root = initialize_Tree( root , "" );       //initialize root
     Tree* tmp = NULL;                          //create tmp to use as a navigation guide through the Tree
     
     //list
     List* head = NULL;                         //create head of the List
     head = initialize_List( head , "" );       //initialize List
     List* last = head;                         //create knob to point at the last element of the List
     
     //filter
     char* filter = NULL;                       //create array of characters to use as Bitmap
     
     filter = initialize_filter(filter , SIZE); //initialize filter (assign filter's size, fill it with zeroes 
     
     //if memory allocation fails
     if( filter == NULL ){
          printf("error at allocating memory for filter\n");
          free_all( root , head , filter , words_array );
          return -1;
     }
     
/////////////////////////////////////////////////////////INITIALIZATIONS//////////////////////////////////////////////////
     

  
/////////////////////////////////////////////////////////TREE ACTION//////////////////////////////////////////////////     
     
     //blossom root
     
     //takes output from oracle, given initial array ("")
     raw_words_array = (char**)oracle( root -> word );
     //throws the words appeared more than once, passing them through the blooom filter
     words_array = filter_words_array( raw_words_array , filter , num_of_hashes , SIZE );
     
     //if first word was the correct one, search is over
     if ( words_array == NULL ){          
          printf("Today it was a good day\nword found at firts attempt *%s*\n", root -> word );
          free_all( root , head , filter , words_array );
          return 0;
     }
     
     //if first word is root and lead at the same time, we cannot continue
     if( words_array[0] == NULL ){
          printf( "unable to build tree because first array is NULL\n" );
          free_all( root , head , filter , words_array );
          return 0;
     }
     
     //if output form oracle was a filled array of pointers, create a new child for every filtered word was returned
     //chech errors for memory allocations
     exit_code = add_new_word_list_T( root , words_array ); error_check( exit_code , root , head , filter , words_array );
     
     //tmp is now the first child of the root
     tmp = root -> child[0];
     
     //while loop stops when the navigation guide reaches the root if the Tree -
     //meaning the Tree is full
     while( tmp != NULL ){
          
          raw_words_array = (char**)oracle( tmp -> word );      //takes new words from oracle
          free_filtered_words( words_array );                   //frees the momory allocated for oracle's output
          
          //passes them through the blooom filter
          words_array = filter_words_array( raw_words_array , filter , num_of_hashes , SIZE );

          //if word was found
          if( words_array == NULL ){
               
               word_found( tmp -> word , "in Tree" , seed , root );     //print aproperate message
               free_all( root , head , filter , words_array );          //free allocated memory
               
               return 0;                                                //exit successfully
          }
          //if first word is NULL - means that either oracle returned NULL itself, or returned no new words
          else if( words_array[0] == NULL ){
               
               //keep asking oracle untill you get a valid array, if the correct word  
               while( words_array != NULL && words_array[0] == NULL ){
                    
                    //tree guide now is the next leaf of the tree,
                    //since the current leaf must not blosson any further
                    tmp = pick_next_leaf( tmp , DEPTH );        
                    
                    //if the root is reached, exit this loop
                    if( tmp == NULL ) break;
                    
                    //else store new oracle's output - asked with new guide's word
                    raw_words_array = (char**)oracle( tmp -> word );    //ask oracle with the guides's word
                    free_filtered_words( words_array );                 //clear previous stored words
                    words_array = filter_words_array( raw_words_array , filter , num_of_hashes , SIZE ); //store filtered words 
               }
               
               //if word was fount
               if( words_array == NULL ){
                    
                    word_found( tmp -> word , "in Tree" , seed , root );        //print aproperate message
                    free_all( root , head , filter , words_array );             //free allocated memory
                    
                    return 0;                                                   //exit successfully
               }
               
               //if the root is reached, exit this loop
               if( tmp == NULL ) break;
          }
          
          //since the program reaches this point, we have valid word sequence to add either in the Tree, or in the List.
          //If limit depth has not been reached, blossom tmp
          if( tmp -> depth < DEPTH ){
               exit_code = add_new_word_list_T( tmp , words_array ); error_check( exit_code , root , head , filter , words_array );
               tmp = pick_next_leaf( tmp , DEPTH );
          }
          //If limit depth has been reached, append words to List
          else{
               last = add_new_word_list_L( last , words_array ); if (last == NULL) error_check( -1 , root , head , filter , words_array );
               tmp = pick_next_leaf( tmp , DEPTH );
          }
          
     }
     
     //Tree is full
//      printf("done with Tree \n");

/////////////////////////////////////////////////////////TREE ACTION//////////////////////////////////////////////////

/////////////////////////////////////////////////////////LIST ACTION//////////////////////////////////////////////////

     head = remove_head( head ); //remove initial "" from List's head

     //while List has valid words to ask oracle
     while( head != NULL ){

          raw_words_array = (char**)oracle( head -> word );     //ask oracle with the word from List's head 
          free_filtered_words( words_array );                   //clear previous stored words
          words_array = filter_words_array( raw_words_array , filter , num_of_hashes , SIZE ); //store filtered words 

          //if word was found 
          if( words_array == NULL ){
               
               word_found( head -> word , "in List ", seed , root );    //print aproperate message
               free_all( root , head , filter , words_array );          //free allocated memory
               
               return 0;                                                //exit successfully
          }
          //if first word is NULL - means that either oracle returned NULL itself, or returned no new words
          else if( words_array[0] == NULL ){
               
               //keep asking oracle untill you get a valid array, if the correct word 
               while( words_array != NULL && words_array[0] == NULL ){

                    //skip head, since head's word is not constructive
                    head = remove_head( head );

                    //if List is eampty, break
                    if ( head == NULL ) break;

                    raw_words_array = (char**)oracle( head -> word );   //else ask for new words from oracle
                    free_filtered_words( words_array );                 //clear previous stored words
                    words_array = filter_words_array( raw_words_array , filter , num_of_hashes , SIZE ); //store filtered words 
               }
               
               //if word is found
               if( words_array == NULL ){
                    word_found( head -> word , "in List " , seed , root);       //print aproperate message
                    free_all( root , head , filter , words_array );             //free allocated memory
                    return 0;                                                //exit successfully
               }
               
               //if List is eampty, break
               if( head == NULL ) break;
          }
          
          //since the program reaches this point, we have valid word sequence to append in the List.
          last = add_new_word_list_L( last , words_array ); if (last == NULL) error_check( -1 , root , head , filter , words_array );
          //remove previous head, since it is tested
          head = remove_head( head );     
     }

     //list is empty
//      printf("done with List \n\n");
     
     //means the word was not found
//      printf("word not found!\n");
     
/////////////////////////////////////////////////////////LIST ACTION//////////////////////////////////////////////////
     
     //free all the memory
     free_all( root , head , filter , words_array );  
     
     return 0;
}
