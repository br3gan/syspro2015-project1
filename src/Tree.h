
#ifndef _TREE_H_
#define _TREE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Tree{
     
     int depth;
     int num_of_children;
     
     char* word;
     
     struct Tree *parent;
     struct Tree **child;

}Tree;

//create a knob with a curtain word, and adds it as a child to parent
Tree* add_child(Tree* parent , const char* word);

//prints only printable characters
void print_cool_chars( char* );

//prints the Tree
//Between two knobs with the same depth, is the subtree of the first
void print_Tree(Tree* root);

//frees all the memory allocated to store the Tree
void free_Tree(Tree* root);

//takes a sequence of words and set each one as children of the knob subtree
int add_new_word_list_T(Tree* subtree, char** words_array);

//initializes the root with a given word, depth 0, number of children = 0
Tree* initialize_Tree( Tree* root , char* word );

//returns the birth order of the next valid child (from the same parent)
//if these is not, returns -1 
//if Parent == NULL ,then tmp = root. returns -2
int has_valid_simbling( Tree* );

//returns a pointer to the next knob which has no children
//(tree fills from left to right) 
Tree* pick_next_leaf( Tree* , int );

#endif