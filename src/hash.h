#ifndef HASH_FUNC_IMPL_H_
#define HASH_FUNC_IMPL_H_

#include <stdint.h>

uint64_t hash_by(int index, const char *word);

#endif
