
#include "Tree.h"

//create a knob with a curtain word, and adds it as a child to parent
Tree* add_child(Tree* parent , const char* word){
     
     int size = 0;
     Tree* new_child;

     //parent will get a new child
     parent -> num_of_children++;

     //allocate memory for the new child
     if ( ( new_child = (Tree*)malloc(sizeof(Tree)) ) == NULL){    
          printf ("(MALLOC)Error at memory allocation for new child\n \t ..will now exit\n");
          return NULL;
     }

     //initialize new child
     new_child -> word = (char*)malloc( strlen(word) + 1);

     strcpy( new_child -> word , word );
     new_child -> depth = parent -> depth + 1;
     new_child -> num_of_children = 0;
     new_child -> child = NULL;
     

     //Parent's children array needs to get one more pointer
     //new size is the old one plus the size of a (Tree*) 
     size = sizeof(Tree*) * ( parent -> num_of_children );

     //reallocate memory to fit new child
     if (( parent -> child = (Tree**)realloc( parent->child , size ) )== NULL){
          printf("(REALLOC)Error at memory reallocation child table\n \t ..will now exit\n");
          return NULL;
     }

     //link parent to child
     (parent -> child[ parent -> num_of_children - 1 ]) = new_child;

     //link child to parent
     new_child -> parent = parent;

     return new_child;
}

//prints only printable characters, for the rest prints the respective asci code of the character
void print_cool_chars( char* string ){
     
     printf("*");
          
     int i = 0;
     
     while( string[i] != '\0' ){
          
          if( ( string[i] >= '0' && string[i] <= '9' )  || 
               (  string[i] >= 'A' && string[i] <= 'Z'  ) || 
               (  (  string[i] >= 'a' && string[i] <= 'z' ) ) ){
                    
               printf("%c", string[i] );
          }
          else
               printf("%d", string[i] );
          
          i++;
     }
     
     printf("*");
     
}

//prints the Tree
//Between two knobs with the same depth, is the subtree of the first
void print_Tree( Tree* root ){
     
     int i = 0; int j = 0;
     
     printf("\n");
     
     
     for( i = 0 ; i < root -> num_of_children ; i++ ){
          
          print_cool_chars( root -> word ); printf(":\t");
          
          print_cool_chars( root -> child[i] -> word );
          
          printf( "\n <- depth/num_of_children : %d/%d ", root -> child[i] -> depth , root -> child[i] -> num_of_children );
          
          for( j = 0 ; j < root -> child[i] -> depth ; j++ ){
               printf("+");
          }
          printf("\n");
          print_Tree( root -> child[i] );
     }     
}

//frees all the memory allocated to store the Tree
void free_Tree(Tree* root){

     int i = root -> num_of_children - 1;

     while ( i >= 0 ){
          free_Tree(root -> child[i]);
          i--;       
     }     
     
     free(root -> word);
     free(root -> child);
     free(root);
}

//takes a sequence of words and set each one as children of the knob subtree
int add_new_word_list_T(Tree* subtree, char** words_array){

     while( words_array[0] ){
     
          Tree* new_child;

          //if first word is valid, it goes to a new born child
          new_child = add_child( subtree , words_array[0] );
          words_array++;
          
          //malloc had failed
          if (new_child == NULL){
               return -1;          
          }     
     }
     
     return 0;
}

//initializes the root with a given word, depth 0, number of children = 0
Tree* initialize_Tree( Tree* root , char* word ){
     
     //root
     root = (Tree*)malloc( sizeof(Tree) );
     if( root == NULL ){
          return (Tree*)NULL;
     }
     
     //word
     root -> word = NULL;     
     root -> word = (char*)malloc( strlen(word) + 1); 
     if( root -> word == NULL ){
          return (Tree*)NULL;
     }
     strcpy(root -> word , word );
     
     //depth
     root -> depth = 0;
     
     //num_of_children
     root -> num_of_children = 0;
     
     //parent
     root -> parent = NULL;
     
     //child
     root -> child = NULL;

     return root;     
}

//returns the number of the next child (from the same parent)
//if these is not, returns -1 
//if Parent == NULL ,then tmp = root. returns -2
int has_valid_simbling( Tree* tmp ){
     
     int i = 0;
     
     //tmp = root
     if(  tmp -> parent == NULL){
          return -2;
     }

     //chechs the memory address of the words to deside if the knobs are the same
     //stops when the same knob is found => i = child_id
     //It is concidered sure that i wont surpass the value [ parent -> num_of_children ]
     while( &(tmp -> word) != &(tmp -> parent -> child[i] -> word) ){
          i++;
     }

     //if the current child is the last of this parent, return -1
     //no valid simblings
     if( i == tmp -> parent -> num_of_children -1 ){
          return -1;
     }
     
     //else return the ID if the child (at which order it was born)
     return i + 1;     
}

//returns a pointer to the next knob which has no children
//(tree fills from left to right) 
Tree* pick_next_leaf( Tree* tmp , int max_depth ){

     //if current knob does have children, return his first child
     if ( tmp -> num_of_children != 0 ){
          return tmp -> child[0];
     }
     
     //else, find his closest non-having-children simbling (from same parent)
     int simbling = has_valid_simbling(tmp);
     
     //if such a knob exists, return it
     if( simbling >= 0 ){          
          return tmp -> parent -> child[simbling];
     }
     
     //else, keep looking at upper level
     while( simbling <= -1 && tmp -> parent != NULL ){
          tmp = tmp -> parent;
          simbling = has_valid_simbling(tmp);
     }
     
     //if search reaches to root, the Tree is full,
     //return NULL
     if(  tmp -> parent == NULL ){
          return NULL;      
     }
     
     //else return propper knob
     tmp = tmp -> parent -> child[simbling];
     
     return tmp;
}