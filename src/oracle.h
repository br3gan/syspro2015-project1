
#ifndef ORACLE_IMPL_H_
#define ORACLE_IMPL_H_

const char **oracle(const char *word);

void initSeed(int seed);

#endif
