
#include <limits.h> //CHAR_BIT
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>

#include "hash.h"
#include "oracle.h"
#include "bloom_filter.h"

//initializes the filter as an array of [SIZE] bytes 
char* initialize_filter( char* filter , unsigned int SIZE ){

     int i = 0;
     
     //allocate memory for the filter
     filter = (char*)malloc( ( SIZE )*sizeof(char) ); 

     //if allocation fails filter = NULL, 
     //else fill the filter with 0s 
     if( filter != NULL )
          for( i = 0 ; i < SIZE ; i++ ){
               filter[i] = 0;
          }
     
     //at any case return a pointer to the filter
     return filter;
}

//adds a hash to the existing filter
char* add_to_filter( char* filter , uint64_t hash , unsigned int SIZE ){

     unsigned char new_byte;
     unsigned int target_byte;
     unsigned char target_bit;
     
     //find the byte that has to change
     target_byte = ( hash%(SIZE * CHAR_BIT) ) / CHAR_BIT;
     //find the bit that has to change
     target_bit = ( hash%(SIZE * CHAR_BIT) ) % CHAR_BIT;
     
     //create the byte with which filter has to be updated with
     new_byte = (1 << target_bit);
     
     //update filter with new byte at the possition of the old one 
     filter[target_byte] = (filter[target_byte] | new_byte);
     
     //return the pointer on the filter
     return filter;
}

//checks if filter has to be updated for curtain hash
int test_hash( char* filter , uint64_t hash , unsigned int SIZE ){

     unsigned char new_byte;
     unsigned int target_byte;
     unsigned char target_bit;
     //find the byte that may has to change
     target_byte = ( hash%(SIZE * CHAR_BIT) ) / CHAR_BIT;
     //find the bit that may has to change
     target_bit = ( hash%(SIZE * CHAR_BIT) ) % CHAR_BIT;
     
     //create the byte with which filter may has to be updated with
     new_byte = (1 << target_bit);
     
     //if filter does not has to be changed return 1
     if ( filter[target_byte] == (filter[target_byte] | new_byte) )
          return 1;
     
     //else return 0
     return 0;     
}

//prints the filter bit by bit. Is not used from this program
void print_filter( char* filter , unsigned int SIZE ){
     
     char tmp_byte[CHAR_BIT];
     int i = 0; int j = 0;
     int tmp;
     
     for( i = 0 ; i < CHAR_BIT ; i++ ){
          tmp_byte[i] = 0;
     }
     
     for( i = SIZE - 1 ; i >= 0 ; i-- ){
          
          tmp = filter[i];
          
          for( j = CHAR_BIT - 1 ; j >= 0 ; j-- ){
               tmp_byte[j] = tmp%2 ;
               tmp = tmp >> 1;
          }
          
          for( j = 0 ; j < CHAR_BIT ; j++ )
               printf("%d ", tmp_byte[j]);
          
          printf(" ");
     }
     
     printf("\n");     
}

//returns 1 if all the [word]'s hashes are contained in the filter
//returns 0 if not   
int contain_in_filter(char* filter , int num_of_hashes , unsigned int SIZE , const char* word ){

     uint64_t hash; 
     
     int hash_num = 1;
     
     //assume word is contained in filter
     int contain = 1;
     
     //for all the hash functions
     for(hash_num = 1 ; hash_num <= num_of_hashes ; hash_num++ ){
      
          //store hash from curtain word
          hash = hash_by( hash_num , word );
     
          //if filter does not contain the hash 
          if( !test_hash(filter , hash , SIZE) ){
               
               //add hash to filter
               filter = add_to_filter(filter , hash , SIZE);
               //assign word as not contained
               contain = 0;
          }
          
          //if variable gets one time zero it will be zero at the return of the function
          //if it gets 1 at last loop, it means filter contained all hashes 
          contain = contain * contain;
     }     
     
     //return info
     return contain;
}

