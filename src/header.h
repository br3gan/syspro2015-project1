
#ifndef _HEADER_H_
#define _HEADER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Tree.h"
#include "List.h"
#include "bloom_filter.h"
#include "oracle.h"

//called when oracle returns NULL pointer
//prints the structure where the word was saved in (tree/list)
//used desides if the tree will be printed
void word_found( char* , char* , int , Tree* );

//asks user a question. Returns 1 or 0,
//if answer is yes or no respectively
int deside( char* );

//allocates memory for the array returned from oracle.
//new array contains not duplicates of a word, nor a word
//which hashes already exist in the filter 
char** filter_words_array( char** , char* , int , unsigned int );

//prints all the arrays in filtered_words_array
//not used in program
void print_filtered_words( char** );

//frees the memory allocated by oracle's copied output
void free_filtered_words( char** );

//trigures all the free functions - frees all the allocated momory
void free_all( Tree* , List* , char* , char** );
     
#endif