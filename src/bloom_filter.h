
#ifndef _BLOOM_FLOOR_H_
#define _BLOOM_FLOOR_H_

#include <limits.h> //CHAR_BIT
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>

#include "hash.h"
#include "oracle.h"

//initializes the filter as an array of [SIZE] bytes 
char* initialize_filter( char* filter , unsigned int SIZE );

//adds a hash to the existing filter
char* add_to_filter( char* filter , uint64_t hash , unsigned int SIZE );

//checks if filter has to be updated for curtain hash
int test_hash( char* filter , uint64_t hash , unsigned int SIZE );

//prints the filter bit by bit. Is not used from this program
void print_filter( char* filter , unsigned int SIZE );

//returns 1 if all the [word]'s hashes are contained in the filter
//returns 0 if not  
int contain_in_filter(char* filter , int num_of_hashes , unsigned int SIZE , const char* word );

#endif