
#include "List.h"
#include "bloom_filter.h"

//creates a knob with a curtain word and adds it at the end of the List
//returns 0 at success
//returns -1 at failure
int add_tail( List* last , char* word ){

     //if List is uninitialized, initialize it
     if( last == NULL ){
          initialize_List(last, word);
          return 0;
     }
     
     List* new_tail = NULL;
     
     //allocate memory for the new knob
     new_tail = (List*)malloc( sizeof(List) );
     //if memmory cannot be allocated, return failure
     if( new_tail == NULL ){
          return -1;
     }
     
     //assign new word
     new_tail -> word = NULL;
     new_tail -> word = (char*)malloc( strlen(word) + 1 ); 
     if( new_tail -> word == NULL ){
          return -1;
     }
     
     strcpy(new_tail -> word , word );
     
     //last knob has no next
     new_tail -> next = NULL;
     
     //previous last has now next the new last
     last -> next = new_tail;
     
     return 0;
}

//delete the first knob, sets nextone as new head
List* remove_head( List* head ){
     
     //if the list is ended, return NULL
     if (head == NULL){
          return NULL;
     }
     
     //else remove knob was pointed by head
     List* to_remove = head;
     
     //make new head
     head = head -> next;
     
     //deallocate memory
     free( to_remove -> word );
     free( to_remove );
     
     //return new head
     return head;
}

//prints List - not used in this program
void print_List( List* head ){
     
     List* tmp = head;
     
     while( tmp != NULL ){
          
          printf(" %s \n", tmp -> word );
          tmp = tmp -> next;
     }
     
}

//frees all the memory allocated to store the List
void free_List( List* head ){
     
     List* tmp;
     
     while( head!= NULL ){
          
          tmp = head;
          head = head -> next;
          remove_head( tmp );
     }
}

//takes a sequence of words and set each one as index of new knobs at the end of the List 
List* add_new_word_list_L( List* last , char** words_array ){
     
     //point to the last knob of the List
     List* new_tail = last;
     
     //as long as words_array is valid
     while( words_array[0] != NULL ){
          
          //create a new knob, with index the first word of the words_array
          if ( add_tail( new_tail , words_array[0] ) == -1 ){
               printf("error at malloc\n");
               return NULL;
          }
          //point to next word
          words_array++;
          //point to new last
          new_tail = new_tail -> next;
     }
     
     //return the very last knob
     return new_tail;     
}

//initializes the List with a given word
List* initialize_List( List* head , char* word ){
     
     //allocate memory for head
     head = (List*)malloc( sizeof(List) );
     
     //if allocation fails
     if( head == NULL ){
          return NULL;
     }
     
     head -> word = NULL;
     
     //allocate memory for head's word
     head -> word = (char*)malloc( strlen(word) + 1 ); 
     if( head -> word == NULL ){
          return NULL;
     }
     
     //copy worn inside head
     strcpy(head -> word , word );
     
     //set next = NULL
     head -> next = NULL;
     
     return head;
}


